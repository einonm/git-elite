git-elite
=========

 A git extension script to give a committer's commit tally as an Elite
 rating, Elite being the popular 80's computer game. The rating
 classifications can be found at:

    http://www.iancgbell.clara.net/elite/faq.htm#A4

 If a name or part of a name is given as a parameter, any committer
 matching is listed, otherwise all committers are listed.

 Author: Mark Einon <mark.einon@gmail.com>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 To use: Place in /usr/local/bin and run as 'git elite [name]'
