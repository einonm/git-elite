#! /bin/sh
#
# A script to annotate a python project with tags and cscope indexes

# etags index
find . -name "*.py" -print | etags -

# cscope for python files (pycscope installed)
find . -name '*.py' > cscope.files 
cscope -R


